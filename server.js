var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var logger = require('morgan');
var config = require('./config.json');
var apiRoutes = express.Router();
var http = require('http');
var sql = require("mssql");
var randomString = require("randomstring");
//var bcrypt = require('bcrypt');
var bcrypt = require('bcrypt-nodejs');
var jwt = require('jsonwebtoken-refresh');
var dateFormat = require('dateformat');
var nodemailer = require('nodemailer');

app.use(express.static(__dirname + '/public')); 	// set the static files location /public/img will be /img for users
app.use(logger('dev'));         					// log every request to the console
app.use(bodyParser.json({ limit: '50mb' }));                           // parse application/json
app.use(bodyParser.urlencoded({ extended: false, limit: '50mb' }));   // parse application/x-www-form-urlencoded

//require('./routes/routes')(app); // configure our routes

// connect the api routes under /api/*
app.use('/api', apiRoutes);

// start app ===============================================
var port = process.env.PORT || 1444;
var server = app.listen(port);

sql.connect(config.database, function (err) {

    if (err) console.log(err);

    apiRoutes.get('/test', function (req, res) {
        res.json({ msg: 'Ok' });
    });

    apiRoutes.post('/registration', function (req, res) {
        try {
            console.log("regisztráció");
            var prefix = req.body.email.split('@')[1];
            if (req.body.isTeacher && prefix != 'inf.u-szeged.hu') {
                res.json({ success: false, errorCode: 2 })
            } else if (req.body.isTeacher == false && prefix != 'stud.u-szeged.hu') {
                res.json({ success: false, errorCode: 2 })
            } else {
                console.log("prefix check ok");
                var request = new sql.Request();
                request.input('email', req.body.email);
                request.query("SELECT id FROM dbo.[user] WHERE email LIKE @email", function (err, result) {
                    if (result.recordset == '') {
                        console.log("email check ok");
                        //const saltRounds = 10;
                        //var salt = bcrypt.genSaltSync(saltRounds);
                        var password = bcrypt.hashSync(req.body.password);
                        var activationString = randomString.generate({ length: 4, charset: 'numeric' });
                        console.log("Ok");
                        var request = new sql.Request();
                        request.input('email', req.body.email);
                        request.input('password', password);
                        request.input('activationCode', activationString);
                        request.input('firstName', req.body.firstName);
                        request.input('lastName', req.body.lastName);
                        request.input('sex', req.body.sex);
                        request.input('dateOfBirth', req.body.dateOfBirth);
                        //request.input('kki', req.body.kki);
                        request.input('isTeacher', req.body.isTeacher);
                        request.input('neptunCode', req.body.neptunCode);
                        request.query("INSERT INTO dbo.[user] ([email], [passwordHash], [activationCode], [activated], [firstName], [lastName], [sex], [dateOfBirth], [kki], [isTeacher], [neptunCode]) VALUES (@email, @password, @activationCode, 0, @firstName, @lastName, @sex, @dateOfBirth, 0.00, @isTeacher, @neptunCode) SELECT SCOPE_IDENTITY() AS insertId", function (err, result) {
                            if (err) {
                                console.log(err);
                                res.json({ success: false, errorCode: 3, msg: 'Server error please try again later.' });
                            } else {
                                nodemailer.createTestAccount((err, account) => {
                                    let transporter = nodemailer.createTransport({
                                        service: 'Gmail',
                                        auth: {
                                            user: 'mmbodomartin@gmail.com',
                                            pass: 'MediaMotionTest1!'
                                        }
                                    });

                                    var mailOptions = {
                                        from: 'mmbodomartin@gmail.com',
                                        to: req.body.email,
                                        subject: 'SZTE kuponok regisztráció',
                                        text: 'Kedves ' + req.body.lastName + ' ' + req.body.firstName  + '!'+ '\n' + 'Köszöntjük az SZTE Kuponok Android alkalmazás felhasználói között. Fiókod aktiválásához kérlek add meg a regisztrációt vagy a bejelentkezést követően a következő kódot: ' + activationString + '\n' + '\n' + 'Üdvözlettel,' + '\n' + 'SZTE Kuponok csapata'
                                    };

                                    transporter.sendMail(mailOptions, function (error, info) {
                                        if (error) {
                                            console.log(error);
                                        } else {
                                            console.log('Email sent: ' + info.response);
                                            console.log("Sikeres regisztráció");
                                            res.json({ success: true });
                                        }
                                    });
                                });
                            }
                        });
                    } else {
                        res.json({ success: false, errorCode: 1, msg: 'Please give an other username' });
                    }
                });
            }
        }
        catch (err) {
            console.log(err);
            res.json({ success: false, errorCode: 3, msg: 'Server error please try again later.' });
        }
    });

    apiRoutes.post('/login', function (req, res) {
        try {
            var request = new sql.Request();
            request.input('email', req.body.email);
            request.query("SELECT * FROM dbo.[user] WHERE email = @email", function (err, userData) {
                if (err) {
                    console.log(err);
                    res.json({ success: false, errorCode: 2, msg: 'Server error. Please try again.' });
                }
                else if (userData.recordset != '') {
                    if (bcrypt.compareSync(req.body.password, userData.recordset[0].passwordHash)) {
                        if (userData.recordset[0].activated == 1) {
                            var date = dateFormat(userData.recordset[0].dateOfBirth, "yyyy-mm-dd");
                            var user = { id: userData.recordset[0].id, userName: userData.recordset[0].userName };
                            var token = jwt.sign(user, config.secretString);
                            console.log('Succesful authentication');
                            if (userData.recordset[0].isTeacher == true) {
                                userData.recordset[0].isTeacher = 1;
                            } else {
                                userData.recordset[0].isTeacher = 0;
                            }
                            res.json({ success: true, errorCode: 0, isTeacher: userData.recordset[0].isTeacher, timestamp: new Date(), token: token, id: userData.recordset[0].id, email: userData.recordset[0].email, firstName: userData.recordset[0].firstName, lastName: userData.recordset[0].lastName, sex: userData.recordset[0].sex, dateOfBirth: date, kki: userData.recordset[0].kki, neptunCode: userData.recordset[0].neptunCode });
                        }
                        else {
                            res.json({ success: false, errorCode: 3, msg: 'Authentication failed. User not activated yet.' });
                        }
                    } else {
                        res.json({ success: false, errorCode: 1, msg: 'Authentication failed. Wrong username or password.' });
                    }
                } else {
                    res.json({ success: false, errorCode: 1, msg: 'Authentication failed. Wrong username or password.' });
                }
            });
        } catch (err) {
            res.json({ success: false, errorCode: 2 });
        }
    });

    apiRoutes.post('/activate', function (req, res) {
        try {
            var request = new sql.Request();
            request.input('email', req.body.email);
            request.query("SELECT activationCode FROM dbo.[user] WHERE email = @email", function (err, userData) {
                if (err) {
                    console.log(err);
                    res.json({ success: false, errorCode: 2, msg: 'Server error. Please try again.' });
                } else {
                    if (userData.recordset == ''){
                        res.json({ success: false, errorCode: 3, msg: 'Not valid email' });
                    } else if (req.body.activationCode == userData.recordset[0].activationCode){
                        request.input('email', req.body.email);
                        request.query("UPDATE dbo.[user] SET activated = 1 WHERE email = @email", function (err, userData) {
                            if (err) {
                                console.log(err);
                                res.json({ success: false, errorCode: 2, msg: 'Server error. Please try again.' });
                            } else {
                                res.json({ success: true });
                            }
                        });
                    } else {
                        res.json({ success: false, errorCode: 4, msg: 'Wrong Code' });
                    }
                }
            });
        } catch (err) {
            res.json({ success: false, errorCode: 2 });
        }
    });

    apiRoutes.post('/getCouponsData', function (req, res) {
        try {
            var token = req.body.token;
            var decoded = jwt.verify(token, config.secretString);
            var request = new sql.Request();
            if (req.body.timestamp == null) {
                request.query("SELECT * FROM dbo.[coupon] WHERE removed = 0", function (err, result) {
                    if (err) {
                        //console.log(err);
                        res.json({ success: false, errorCode: 1, msg: 'Server error. Please try again.' });
                    } else {
                        //console.log(result.recordset.length)
                        var finishedRequests = 0;
                        for (let i = 0; i < result.recordset.length; i++) {
                            var request = new sql.Request();
                            request.input('couponId', result.recordset[i].id);
                            request.input('userId', decoded.id);
                            request.query("SELECT id FROM dbo.[favourite] WHERE couponId = @couponId AND userId = @userId", function (err, result2) {
                                if (err) {
                                    //console.log(err);
                                    res.json({ success: false, errorCode: 1, msg: 'Server error. Please try again.' });
                                } else {
                                    //console.log(result.recordset[i]);
                                    //console.log(result.recordset[i].isForTeachers);
                                    if (result2.recordset[i] != null) {
                                        result.recordset[i].isFavourite = 1;
                                    } else {
                                        //console.log("Ok");
                                        result.recordset[i].isFavourite = 0;
                                    } if (result.recordset[i].isForTeachers == true) {
                                        //console.log("teacher");
                                        result.recordset[i].isForTeachers = 1;
                                    } else {
                                        result.recordset[i].isForTeachers = 0;
                                    } if (result.recordset[i].isForStudents == true) {
                                        result.recordset[i].isForStudents = 1;
                                    } else {
                                        result.recordset[i].isForStudents = 0;
                                    }
                                    //console.log(result.recordset[i]);
                                }
                                finishedRequests++;
                                //console.log(finishedRequests);
                                if (finishedRequests == result.recordset.length) {
                                    console.log("End");
                                    res.json({ success: true, couponsData: result.recordset })
                                }
                            });
                        }
                    }
                });
            } else {
                request.input('timestamp', req.body.timestamp);
                request.query("SELECT * FROM dbo.[coupon] WHERE removed = 0 AND lastUpdatedDate >= @timestamp", function (err, result) {
                    if (err) {
                        console.log(err);
                        res.json({ success: false, errorCode: 1, msg: 'Server error. Please try again.' });
                    } else {
                        if (result.recordset == '') {
                            res.json({ success: true, couponsData: [] });
                        } else {
                            var finishedRequests = 0;
                            for (let i = 0; i < result.recordset.length; i++) {
                                request.input('couponId', result.recordset[i].id);
                                request.input('userId', decoded.id);
                                request.query("SELECT id FROM dbo.[favourite] WHERE couponId = @couponId AND userId = @userId", function (err, result2) {
                                    if (err) {
                                        console.log(err);
                                        res.json({ success: false, errorCode: 2, msg: 'Server error. Please try again.' });
                                    } else {
                                        if (result2.recordset[i] != null) {
                                            result.recordset[i].isFavourite = 1;
                                        } else {
                                            result.recordset[i].isFavourite = 0;
                                        }
                                    }
                                    finishedRequests++;
                                    console.log(finishedRequests);
                                    if (finishedRequests == result.recordset.length) {
                                        console.log("End");
                                        res.json({ success: true, couponsData: result.recordset })
                                    }
                                });
                            }
                        }
                    }
                });
            }
        } catch (err) {
            res.json({ errorCode: 2 });
        }

    });

    apiRoutes.post('/getInstitutionsData', function (req, res) {
        try {
            var token = req.body.token;
            var decoded = jwt.verify(token, config.secretString);
            var request = new sql.Request();
            if (req.body.timestamp == null) {
                request.query("SELECT * FROM dbo.[institution] WHERE removed = 0", function (err, result) {
                    if (err) {
                        console.log(err);
                        res.json({ success: false, errorCode: 1, msg: 'Server error. Please try again.' });
                    } else {
                        res.json({ success: true, institutionsData: result.recordset })
                    }
                });
            } else {
                request.input('timestamp', req.body.timestamp);
                request.query("SELECT * FROM dbo.[institution] WHERE removed = 0 AND lastUpdatedDate >= @timestamp", function (err, result) {
                    if (err) {
                        console.log(err);
                        res.json({ success: false, errorCode: 1, msg: 'Server error. Please try again.' });
                    } else {
                        res.json({ success: true, institutionsData: result.recordset })
                    }
                });
            }
        } catch (err) {
            res.json({ errorCode: 2 });
        }
    });

    apiRoutes.post('/getRedeemPlace', function (req, res) {
        try {
            var token = req.body.token;
            var decoded = jwt.verify(token, config.secretString);
            var request = new sql.Request();
            if (req.body.timestamp == null) {
                request.query("SELECT * FROM dbo.[redeemPlace] WHERE removed = 0", function (err, result) {
                    if (err) {
                        console.log(err);
                        res.json({ success: false, errorCode: 1, msg: 'Server error. Please try again.' });
                    } else {
                        res.json({ success: true, redeemPlace: result.recordset });
                    }
                });
            } else {
                request.input('timestamp', req.body.timestamp);
                request.query("SELECT * FROM dbo.[redeemPlace] WHERE removed = 0 AND lastUpdatedDate >= @timestamp", function (err, result) {
                    if (err) {
                        console.log(err);
                        res.json({ success: false, errorCode: 1, msg: 'Server error. Please try again.' });
                    } else {
                        res.json({ success: true, redeemPlace: result.recordset });
                    }
                });
            }
        } catch (err) {
            res.json({ errorCode: 2 });
        }
    });

    apiRoutes.post('/getRedeemPlaces', function (req, res) {
        try {
            var token = req.body.token;
            var decoded = jwt.verify(token, config.secretString);
            var request = new sql.Request();
            if (req.body.timestamp == null) {
                request.query("SELECT * FROM dbo.[redeemPlaces] WHERE removed = 0", function (err, result) {
                    if (err) {
                        console.log(err);
                        res.json({ success: false, errorCode: 1, msg: 'Server error. Please try again.' });
                    } else {
                        res.json({ success: true, redeemPlaces: result.recordset });
                    }
                });
            } else {
                request.input('timestamp', req.body.timestamp);
                request.query("SELECT * FROM dbo.[redeemPlaces] WHERE removed = 0 AND lastUpdatedTimestamp >= @timestamp", function (err, result) {
                    if (err) {
                        console.log(err);
                        res.json({ success: false, errorCode: 1, msg: 'Server error. Please try again.' });
                    } else {
                        res.json({ success: true, redeemPlaces: result.recordset });
                    }
                });
            }
        } catch (err) {
            res.json({ errorCode: 2 });
        }
    });

    apiRoutes.post('/setFavouriteCoupon', function (req, res) {
        try {
            var token = req.body.token;
            var decoded = jwt.verify(token, config.secretString);
            var request = new sql.Request();
            request.input('couponId', req.body.couponId);
            request.input('userId', decoded.id);
            request.query("SELECT * FROM dbo.[favourite] WHERE couponId = @couponId AND userId = @userId", function (err, result) {
                if (err) {
                    console.log(err);
                    res.json({ success: false, errorCode: 1, msg: 'Server error. Please try again.' });
                } else {
                    if (result.recordset == '') {
                        request.input('couponId', req.body.couponId);
                        request.input('userId', decoded.id);
                        request.query("INSERT INTO dbo.[favourite] (userId, couponId) VALUES (@userId, @couponId)", function (err, result) {
                            if (err) {
                                console.log(err);
                                res.json({ success: false, errorCode: 1, msg: 'Server error. Please try again.' });
                            } else {
                                res.json({ success: true });
                            }
                        });
                    } else {
                        res.json({ success: true, errorCode: 3, msg: 'Already added to favourite.' });
                    }
                }
            });
        } catch (err) {
            res.json({ errorCode: 2 });
        }
    });

    apiRoutes.post('/removeFavouriteCoupon', function (req, res) {
        try {
            var token = req.body.token;
            var decoded = jwt.verify(token, config.secretString);
            var request = new sql.Request();
            request.input('couponId', req.body.couponId);
            request.input('userId', decoded.id);
            request.query("DELETE FROM dbo.[favourite] WHERE couponId = @couponId AND userId = @userId", function (err, result) {
                if (err) {
                    console.log(err);
                    res.json({ success: false, errorCode: 1, msg: 'Server error. Please try again.' });
                } else {
                    res.json({ success: true });
                }
            });
        } catch (err) {
            res.json({ errorCode: 2 });
        }
    });

    apiRoutes.post('/getFavouriteCoupons', function (req, res) {
        try {
            var token = req.body.token;
            var decoded = jwt.verify(token, config.secretString);
            var request = new sql.Request();
            request.input('userId', decoded.id);
            request.query("SELECT couponId FROM dbo.[favourite] WHERE userId = @userId", function (err, result) {
                if (err) {
                    console.log(err);
                    res.json({ success: false, errorCode: 1, msg: 'Server error. Please try again.' });
                } else {
                    res.json({ success: true, favouriteCoupons: result.recordset });
                }
            });
        } catch (err) {
            res.json({ errorCode: 2 });
        }
    });

    apiRoutes.post('/redeemCoupon', function (req, res) {
        try {
            var token = req.body.token;
            var decoded = jwt.verify(token, config.secretString);
            var sem = require('semaphore')(1);
            var request = new sql.Request();
            var date = new Date();
            var finishedRequests = 0;
            var canReturn = true;
            sem.take(function () {
                request.input('couponId', req.body.couponId);
                request.query("SELECT * FROM dbo.[coupon] WHERE id = @couponId", function (err, result) {
                    if (err) {
                        console.log(err);
                        res.json({ success: false, errorCode: 1, msg: 'Server error. Please try again.' });
                    } else {
                        request.input('userId', decoded.id);
                        request.query("SELECT kki, isTeacher FROM dbo.[user] WHERE id = @userId", function (err, result2) {
                            if (err) {
                                console.log(err);
                                res.json({ success: false, errorCode: 1, msg: 'Server error. Please try again.' });
                            } else {
                                if (result.recordset[0].minKki != 0 || result.recordset[0].maxKki != 10) {
                                    if (result2.recordset[0].kki >= result.recordset[0].minKki && result2.recordset[0].kki <= result.recordset[0].maxKki) {
                                        finishedRequests++;
                                        if (finishedRequests == 5) {
                                            sem.leave();
                                        }
                                    } else {
                                        if (canReturn) {
                                            canReturn = false;
                                            res.json({ success: false, errorCode: 3, msg: 'Invalid kki.' });
                                        }
                                    }
                                } else {
                                    finishedRequests++;
                                    if (finishedRequests == 5) {
                                        sem.leave();
                                    }
                                }
                                if ((result2.recordset[0].isTeacher == true && result.recordset[0].isForTeachers == true) || (result2.recordset[0].isTeacher == false && result.recordset[0].isForStudents == true)) {
                                    finishedRequests++;
                                    if (finishedRequests == 5) {
                                        sem.leave();
                                    }
                                } else {
                                    if (canReturn) {
                                        canReturn = false;
                                        res.json({ success: false, errorCode: 4, msg: 'Invalid usage.' });
                                    }
                                }
                                console.log(result.recordset[0].redeemStartDate);
                                console.log(result.recordset[0].redeemEndDate);
                                var currentDate = dateFormat(date, "yyyy-mm-dd");
                                if (currentDate >= result.recordset[0].redeemStartDate && currentDate <= result.recordset[0].redeemEndDate) {
                                    finishedRequests++;
                                    if (finishedRequests == 5) {
                                        sem.leave();
                                    }
                                } else {
                                    if (canReturn) {
                                        canReturn = false;
                                        res.json({ success: false, errorCode: 5, msg: 'Not redeemable coupon already/yet' });
                                    }
                                }
                                if (result.recordset[0].OverallLimitation == 'limited') {
                                    request.input('couponId', req.body.couponId);
                                    request.query("SELECT COUNT(id) AS sum FROM dbo.[redeems] WHERE couponId = @couponId", function (err, result3) {
                                        if (err) {
                                            console.log(err);
                                            res.json({ success: false, errorCode: 1, msg: 'Server error. Please try again.' });
                                        } else {
                                            if (result.recordset[0].OverallAmount > result3.recordset[0].sum) {
                                                finishedRequests++;
                                                if (finishedRequests == 5) {
                                                    sem.leave();
                                                }
                                            } else {
                                                if (canReturn) {
                                                    canReturn = false;
                                                    res.json({ success: false, errorCode: 6, msg: 'In total the coupon sold out', redeemed: result.recordset[0].OverallAmount });
                                                }
                                            }
                                        }
                                    });
                                } else {
                                    finishedRequests++;
                                    if (finishedRequests == 5) {
                                        sem.leave();
                                    }
                                }
                                if (result.recordset[0].PerUserLimitation == 'limited') {
                                    request.input('userId', decoded.id);
                                    console.log(req.body.couponId);
                                    request.input('couponId', req.body.couponId);
                                    request.query("SELECT COUNT(id) AS sum FROM dbo.[redeems] WHERE couponId = @couponId AND userId = @userId", function (err, result4) {
                                        if (err) {
                                            console.log(err);
                                            res.json({ success: false, errorCode: 1, msg: 'Server error. Please try again.' });
                                        } else {
                                            console.log(result4.recordset[0].sum);
                                            if (result.recordset[0].PerUserAmount > result4.recordset[0].sum) {
                                                finishedRequests++;
                                                if (finishedRequests == 5) {
                                                    sem.leave();
                                                }
                                            } else {
                                                request.input('userId', decoded.id);
                                                request.input('couponId', req.body.couponId);
                                                request.query("SELECT TOP 1 redeemDate FROM dbo.[redeems] WHERE couponId = couponId AND userId = @userId ORDER BY redeemDate DESC", function (err, result5) {
                                                    if (err) {
                                                        console.log(err);
                                                        res.json({ success: false, errorCode: 1, msg: 'Server error. Please try again.' });
                                                    } else {
                                                        if (canReturn) {
                                                            canReturn = false;
                                                            console.log(result5.recordset);
                                                            res.json({ success: false, errorCode: 7, msg: 'The coupon for this user sold out', lastRedeemDate: result5.recordset[0].redeemDate, redeemed: result.recordset[0].PerUserAmount });
                                                        }
                                                    }
                                                });
                                            }
                                        }
                                    });
                                } else {
                                    finishedRequests++;
                                    if (finishedRequests == 5) {
                                        sem.leave();
                                    }
                                }
                            }
                        });
                    }
                });
            });
            sem.take(function () {
                console.log(finishedRequests);
                if (finishedRequests == 5) {
                    var date2 = new Date();
                    var currentDate = dateFormat(date2, "yyyy-mm-dd");
                    request.input('userId', decoded.id);
                    request.input('couponId', req.body.couponId);
                    request.input('date', currentDate);
                    request.query("INSERT INTO dbo.[redeems] (userId, couponId, redeemDate) VALUES (@userId, @couponId, @date) SELECT SCOPE_IDENTITY() AS insertId", function (err, result) {
                        if (err) {
                            console.log(err);
                            res.json({ success: false, errorCode: 1, msg: 'Server error. Please try again.' });
                        } else {
                            res.json({ success: true, serverTimestamp: currentDate, id: result.recordset[0].insertId });
                        }
                    });
                } else {
                    res.json({ success: false, errorCode: 1, msg: 'Server error. Please try again.' });
                }
            });
        } catch (err) {
            res.json({ errorCode: 2 });
        }
    });

    apiRoutes.post('/getRedeemsData', function (req, res) {
        try {
            var token = req.body.token;
            var decoded = jwt.verify(token, config.secretString);
            var request = new sql.Request();
            if (req.body.timestamp == null) {
                request.input('userId', decoded.id);
                request.query("SELECT * FROM dbo.[redeems] WHERE userId = @userId", function (err, result) {
                    if (err) {
                        console.log(err);
                        res.json({ success: false, errorCode: 1, msg: 'Server error. Please try again.' });
                    } else {
                        res.json({ success: true, redeems: result.recordset });
                    }
                });
            } else {
                request.input('userId', decoded.id);
                request.input('timestamp', req.body.timestamp);
                request.query("SELECT * FROM dbo.[redeems] WHERE userId = @userId AND redeemDate >= @timestamp", function (err, result) {
                    if (err) {
                        console.log(err);
                        res.json({ success: false, errorCode: 1, msg: 'Server error. Please try again.' });
                    } else {
                        res.json({ success: true, redeems: result.recordset });
                    }
                });
            }
        } catch (err) {
            res.json({ errorCode: 2 });
        }
    });
});

//exports = module.exports = app;						// expose app

